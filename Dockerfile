ARG BASE_IMAGE
FROM $BASE_IMAGE

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

COPY docker-requirements.txt /

RUN set -x \
&& export DEBIAN_FRONTEND=noninteractive \
&& apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y git gcc \
&& pip install -r /docker-requirements.txt \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
;
